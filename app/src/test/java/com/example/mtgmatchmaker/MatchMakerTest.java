package com.example.mtgmatchmaker;

import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;
import com.example.mtgmatchmaker.view.gamelobby.matching.MatchMaker;
import com.example.mtgmatchmaker.view.gamelobby.matching.Pairing;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MatchMakerTest extends TestCase {
    private String[] playersArr = new String[]{"player1", "player2", "player3", "player4", "player5"};
    private List<GamePlayerCrossRef> playersList= new ArrayList<>(Arrays.asList(
        new GamePlayerCrossRef("player1", "testGame", "this is a picture"),
        new GamePlayerCrossRef("player2", "testGame", "this is a picture"),
        new GamePlayerCrossRef("player3", "testGame", "this is a picture"),
        new GamePlayerCrossRef("player4", "testGame", "this is a picture"),
        new GamePlayerCrossRef("player5", "testGame", "this is a picture")
    ));
    private List<Pairing> result =new ArrayList<>(Arrays.asList(
            new Pairing("player4", "player2"),
            new Pairing("player1", "player5")
    ));
    private List<Pairing> result2 =new ArrayList<>(Arrays.asList(
            new Pairing("player2", "player5"),
            new Pairing("player4", "player3")
    ));

    @Test
    public void testRoundRobinPairing() {
        MatchMaker matchMaker = new MatchMaker(playersArr, true, 0);
        List<Pairing> pairings = matchMaker.match(playersList);
        assertEquals(2, pairings.size());
        for(int i = 0; i<pairings.size(); i++){
            Assert.assertEquals( result.get(i).getPlayer1(), pairings.get(i).getPlayer1());
            Assert.assertEquals(result.get(i).getPlayer2(), pairings.get(i).getPlayer2());
        }
    }

    @Test
    public void testRoundRobinNextRound(){
        MatchMaker matchmaker = new MatchMaker(playersArr, true, 0);
        List<Pairing> pairingsRound1 = matchmaker.match(playersList);
        List<Pairing> pairingsRound2 = matchmaker.match(playersList);
        for(int i = 0; i<pairingsRound2.size(); i++){
            Assert.assertEquals( result2.get(i).getPlayer1(), pairingsRound2.get(i).getPlayer1());
            Assert.assertEquals( result2.get(i).getPlayer2(), pairingsRound2.get(i).getPlayer2());
        }
        assertEquals( 2, pairingsRound2.size());
    }

    @Test
    public void testDoubleEliminationPairing(){
        MatchMaker matchMaker = new MatchMaker(playersArr, false, 0);
        List<Pairing> pairings = matchMaker.match(playersList);
        assertEquals(pairings.size(), 2);
        for(int i = 0; i<pairings.size(); i++){
            Assert.assertEquals(result.get(i).getPlayer1(), pairings.get(i).getPlayer1());
            Assert.assertEquals(result.get(i).getPlayer2(), pairings.get(i).getPlayer2());
        }

        playersList.get(0).setPoints(2);
        playersList.get(1).setPoints(1);
        playersList.get(2).setPoints(1);
        List<Pairing> secondRoundPairings = matchMaker.match(playersList);
        assertEquals(2, secondRoundPairings.size());
        assertEquals("player4", secondRoundPairings.get(0).getPlayer1());
        assertEquals("player5", secondRoundPairings.get(0).getPlayer2());
        assertEquals("player2", secondRoundPairings.get(1).getPlayer1());
        assertEquals("player3", secondRoundPairings.get(1).getPlayer2());
    }
}