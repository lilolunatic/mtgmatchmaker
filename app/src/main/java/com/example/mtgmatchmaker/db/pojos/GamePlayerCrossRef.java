package com.example.mtgmatchmaker.db.pojos;

import androidx.annotation.NonNull;
import androidx.room.*;

@Entity(indices = {@Index("gameName"), @Index("playerName")},
        tableName = "game_player_assignment",
        primaryKeys = {"gameName", "playerName"},
        foreignKeys = {
                @ForeignKey(
                        entity = Game.class,
                        parentColumns = "gameName",
                        childColumns = "gameName"
                ),
                @ForeignKey(
                        entity = Player.class,
                        parentColumns = "playerName",
                        childColumns = "playerName"
                )
        })

public class GamePlayerCrossRef implements Comparable<GamePlayerCrossRef> {


    @NonNull
    private final String gameName;
    @NonNull
    private final String playerName;
    private String pictureUri;
    private int points;
    private int timesMatched;

    public GamePlayerCrossRef(@NonNull String playerName, @NonNull String gameName, String pictureUri){
        this.gameName = gameName;
        this.playerName = playerName;
        this.pictureUri = pictureUri;
        points = 0;
        timesMatched = 0;
    }

    @Override
    public int compareTo(GamePlayerCrossRef o) {
        return (this.points - o.points);
    }

    @NonNull
    public String getGameName() {
        return gameName;
    }

    @NonNull
    public String getPlayerName() {
        return playerName;
    }

    public String getPictureUri() {
        return pictureUri;
    }

    public int getPoints() {
        return points;
    }

    public int getTimesMatched() {
        return timesMatched;
    }

    public void setPictureUri(String pictureUri) {
        this.pictureUri = pictureUri;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setTimesMatched(int timesMatched) {
        this.timesMatched = timesMatched;
    }
}

