package com.example.mtgmatchmaker.db.pojos;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "player_table")
public class Player {
    @PrimaryKey
    @NonNull
    private final String playerName;

    public Player(@NonNull String playerName) {this.playerName = playerName;}
    @NonNull
    public String getPlayerName(){return this.playerName;}
}
