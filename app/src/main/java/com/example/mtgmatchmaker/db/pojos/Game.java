package com.example.mtgmatchmaker.db.pojos;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "game_table")
public class Game {
    @PrimaryKey
    @NonNull
    private final String gameName;
    private final boolean isRoundRobin;
    private int roundCount;

    public Game(@NonNull String gameName, boolean isRoundRobin) {
        this.gameName = gameName;
        this.isRoundRobin = isRoundRobin;
        roundCount = 0;
    }

    @NonNull
    public String getGameName(){ return this.gameName; }

    public boolean isRoundRobin(){return this.isRoundRobin;}


    public int getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }
}
