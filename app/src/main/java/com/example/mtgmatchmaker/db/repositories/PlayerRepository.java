package com.example.mtgmatchmaker.db.repositories;

import androidx.lifecycle.LiveData;

import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.daos.PlayerDao;
import com.example.mtgmatchmaker.db.pojos.Player;

import java.util.List;

public class PlayerRepository {
    private final PlayerDao mPlayerDao;
    private final LiveData<List<Player>> mAllPlayers;

    public PlayerRepository(MatchmakerDatabase db){
        mPlayerDao = db.playerDao();
        mAllPlayers = mPlayerDao.getPlayers();
    }

    public void insert(Player player){
        MatchmakerDatabase.databaseWriteExecutor.execute(()-> mPlayerDao.insert(player));
    }

    public LiveData<List<Player>> getAllPlayers(){return mAllPlayers;}
}
