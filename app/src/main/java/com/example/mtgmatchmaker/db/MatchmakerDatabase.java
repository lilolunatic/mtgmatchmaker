package com.example.mtgmatchmaker.db;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import com.example.mtgmatchmaker.db.daos.GameDao;
import com.example.mtgmatchmaker.db.daos.GameWithPlayerDao;
import com.example.mtgmatchmaker.db.daos.PlayerDao;
import com.example.mtgmatchmaker.db.pojos.Game;
import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;
import com.example.mtgmatchmaker.db.pojos.Player;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Game.class, Player.class, GamePlayerCrossRef.class}, version = 1, exportSchema= false)
public abstract class MatchmakerDatabase extends RoomDatabase {
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private static volatile MatchmakerDatabase INSTANCE;

    public abstract GameDao gameDao();
    public abstract PlayerDao playerDao();
    public abstract GameWithPlayerDao gamesWithPlayersDao();

    public static MatchmakerDatabase getDatabase(final Context context){
        if(INSTANCE == null){
            synchronized (MatchmakerDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MatchmakerDatabase.class, "matchmaker_database").build();
                }
            }
        }
        return INSTANCE;
    }
}
