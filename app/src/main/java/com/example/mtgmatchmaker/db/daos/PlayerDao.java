package com.example.mtgmatchmaker.db.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.example.mtgmatchmaker.db.pojos.Player;

import java.util.List;

@Dao
public interface PlayerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Player player);

    @Query("DELETE FROM player_table")
    void deleteAll();

    @Query("SELECT * FROM player_table")
    LiveData<List<Player>> getPlayers();

    @Query("SELECT * FROM player_table WHERE playerName = :player_name")
    Player getPlayer(String player_name);
}
