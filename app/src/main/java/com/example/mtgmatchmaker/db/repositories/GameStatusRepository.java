package com.example.mtgmatchmaker.db.repositories;

import android.os.Build;

import androidx.lifecycle.LiveData;

import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.daos.GameWithPlayerDao;
import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;
import com.example.mtgmatchmaker.view.gamecreation.PlayerHelper;

import java.util.List;

public class GameStatusRepository {
    private final GameWithPlayerDao mGameWithPlayersDao;

    public GameStatusRepository(MatchmakerDatabase db){
        mGameWithPlayersDao = db.gamesWithPlayersDao();
    }

    public LiveData<Integer> getPlayerCount(String gameName){return mGameWithPlayersDao.getPlayerCount(gameName);}

    public LiveData<List<GamePlayerCrossRef>> getRoundRobinRanking(String gameName){
        return mGameWithPlayersDao.getRankingByPoints(gameName);
    }

    public LiveData<Integer> getDoubleEliminationPlayerCount(String gameName){
        return mGameWithPlayersDao.getDoubleEliminationPlayerCount(gameName);
    }

    public LiveData<List<GamePlayerCrossRef>> getDoubleEliminationRanking(String gameName){
        return mGameWithPlayersDao.getRankingDoubleElimination(gameName);
    }

    public LiveData<List<GamePlayerCrossRef>> getGameStatus(String gameName){
        return mGameWithPlayersDao.getGameStatus(gameName);
    }

    public LiveData<List<String>> loadGameData(String gameName){
        return mGameWithPlayersDao.loadGameData(gameName);
    }

    public void insertPlayersOfGame(List<PlayerHelper> addedPlayers, String gameName) {
        MatchmakerDatabase.databaseWriteExecutor.execute(() ->{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                addedPlayers.forEach(player ->{
                    mGameWithPlayersDao.insert(
                            new GamePlayerCrossRef(player.getPlayerName(), gameName, player.getPictureUri()));
                });
            }else {
                for (int i = 0; i < addedPlayers.size(); i++) {
                    mGameWithPlayersDao.insert(new GamePlayerCrossRef(
                            addedPlayers.get(i).getPlayerName(),
                            gameName,
                            addedPlayers.get(i).getPictureUri()));
                }
            }
        });
    }

    public void increaseTimesMatched(String gameName, String playerName){
        MatchmakerDatabase.databaseWriteExecutor.execute(() -> mGameWithPlayersDao.addMatch(gameName, playerName));
    }

    public void addPoints(String gameName, String playerName, int points){
        MatchmakerDatabase.databaseWriteExecutor.execute(() -> mGameWithPlayersDao.addPoints(gameName, playerName, points));
    }
}
