package com.example.mtgmatchmaker.db.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.example.mtgmatchmaker.db.pojos.Game;

import java.util.List;

@Dao
public interface GameDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Game game);

    @Query("SELECT * FROM game_table ORDER BY gameName ASC")
    LiveData<List<Game>> getOrderedGames();

    @Query("SELECT * FROM game_table WHERE gameName= :gameName")
    LiveData<Game> getGame(String gameName);

    @Query("UPDATE game_table SET roundCount = roundCount + 1 WHERE gameName = :gameName")
    void nextRound(String gameName);

    @Query("SELECT roundCount FROM game_table WHERE gameName = :gameName")
    LiveData<Integer> getRoundCount(String gameName);

}
