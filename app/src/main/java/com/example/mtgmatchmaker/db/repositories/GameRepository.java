package com.example.mtgmatchmaker.db.repositories;

import androidx.lifecycle.LiveData;

import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.daos.GameDao;
import com.example.mtgmatchmaker.db.pojos.Game;

import java.util.List;

public class GameRepository {
    private final GameDao mGameDao;
    private final LiveData<List<Game>> mAllGames;

    public GameRepository(MatchmakerDatabase db){
        mGameDao = db.gameDao();
        mAllGames = mGameDao.getOrderedGames();
    }

    public LiveData<List<Game>> getAllGames(){
        return mAllGames;
    }
    public LiveData<Game> getGame(String gameName){return mGameDao.getGame(gameName);}
    public LiveData<Integer> getRoundCount(String gameName){return mGameDao.getRoundCount(gameName);}
    public void increaseRoundCount(String gameName){
        MatchmakerDatabase.databaseWriteExecutor.execute(() -> mGameDao.nextRound(gameName));
    }
    public void insert(Game game){
        MatchmakerDatabase.databaseWriteExecutor.execute(() -> mGameDao.insert(game));
    }
}
