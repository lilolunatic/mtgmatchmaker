package com.example.mtgmatchmaker.db.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;

import java.util.List;


@Dao
public interface GameWithPlayerDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(GamePlayerCrossRef gamePlayerCrossRef);

    @Query("SELECT * FROM GAME_PLAYER_ASSIGNMENT WHERE gameName = :gameName")
    LiveData<List<GamePlayerCrossRef>> getGameStatus(String gameName);

    @Query("SELECT playerName FROM GAME_PLAYER_ASSIGNMENT WHERE gameName = :gameName")
    LiveData<List<String>> loadGameData(String gameName);

    @Query("UPDATE GAME_PLAYER_ASSIGNMENT SET points = points + :points WHERE gameName = :gameName " +
            "AND playerName = :playerName")
    void addPoints(String gameName, String playerName, int points);

    @Query("UPDATE game_player_assignment SET timesMatched = timesMatched + 1 WHERE gameName = :gameName " +
            "AND playerName = :playerName")
    void addMatch(String gameName, String playerName);

    @Query("SELECT * FROM GAME_PLAYER_ASSIGNMENT WHERE gameName = :gameName ORDER BY points DESC")
    LiveData<List<GamePlayerCrossRef>> getRankingByPoints(String gameName);

    @Query("SELECT playerName, pictureUri, gameName, timesMatched, (timesMatched - points) as points " +
            "FROM GAME_PLAYER_ASSIGNMENT WHERE gameName = :gameName ORDER BY (timesMatched-points) DESC")
    LiveData<List<GamePlayerCrossRef>> getRankingDoubleElimination(String gameName);

    @Query("SELECT COUNT(playerName) FROM GAME_PLAYER_ASSIGNMENT WHERE gameName = :gameName")
    LiveData<Integer> getPlayerCount(String gameName);

    @Query("SELECT COUNT(playerName) FROM GAME_PLAYER_ASSIGNMENT WHERE gameName = :gameName AND points < 2")
    LiveData<Integer> getDoubleEliminationPlayerCount(String gameName);
}


