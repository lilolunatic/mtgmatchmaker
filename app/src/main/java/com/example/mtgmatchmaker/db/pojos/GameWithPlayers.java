package com.example.mtgmatchmaker.db.pojos;

import androidx.room.*;

import java.util.List;

public class GameWithPlayers {

    @Embedded
    public Game game;

    @Relation(
            parentColumn = "gameName",
            entityColumn = "playerName",
            associateBy = @Junction(GamePlayerCrossRef.class)
    )
    public List<GamePlayerCrossRef> gamePlayerCrossRefs;
}

