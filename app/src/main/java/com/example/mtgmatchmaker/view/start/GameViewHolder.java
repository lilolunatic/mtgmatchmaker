package com.example.mtgmatchmaker.view.start;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mtgmatchmaker.R;
import com.example.mtgmatchmaker.view.scoring.ListItemClickListener;

public class GameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final TextView gameItemView;
    private final ListItemClickListener listItemClickListener;

    private GameViewHolder(View itemView, ListItemClickListener listItemClickListener){
        super(itemView);
        itemView.setOnClickListener(this);
        this.listItemClickListener = listItemClickListener;
        gameItemView = itemView.findViewById(R.id.textView);
    }

    public void bind(String text){
        gameItemView.setText(text);
    }

    public static GameViewHolder create(ViewGroup parent, ListItemClickListener listItemClickListener){
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item, parent, false);
        return new GameViewHolder(view, listItemClickListener);
    }

    @Override
    public void onClick(View v){
        int position = getAdapterPosition();
        listItemClickListener.onClickListItem(position);
    }
}
