package com.example.mtgmatchmaker.view.gamelobby;

import android.net.Uri;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mtgmatchmaker.view.gamelobby.matching.Pairing;
import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;

import java.util.List;

public class MatchListAdapter extends RecyclerView.Adapter<MatchRowViewHolder> {
    private final List<GamePlayerCrossRef> players;
    private final List<Pairing> pairings;

    public MatchListAdapter(List<Pairing> pairings, List<GamePlayerCrossRef> players) {
        this.players = players;
        this.pairings = pairings;
    }

    @NonNull
    @Override
    public MatchRowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return MatchRowViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchRowViewHolder holder, int position) {
        GamePlayerCrossRef player1 = null;
        GamePlayerCrossRef player2 = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N && position < pairings.size()) {
            player1 = players.stream()
                    .filter(p -> p.getPlayerName().equals(pairings.get(Integer.min(position, pairings.size()-1)).getPlayer1()))
                    .findFirst()
                    .orElse(null);

            player2 = players.stream()
                    .filter(p -> p.getPlayerName().equals(pairings.get(Integer.min(position, pairings.size() -1)).getPlayer2()))
                    .findFirst()
                    .orElse(null);

            holder.bind(player1.getPlayerName(), player2.getPlayerName(), player1.getPoints(), player2.getPoints(),
                    Uri.parse(player1.getPictureUri()), Uri.parse(player2.getPictureUri() ));
        } else {
            for(int i = 0; i< players.size(); i++){
                if(players.get(i).getPlayerName() == pairings.get(
                        getPairingPosition(position, pairings.size())).getPlayer1()){
                    player1 = players.get(i);
                }
                if(players.get(i).getPlayerName() == pairings.get(
                        getPairingPosition(position, pairings.size())).getPlayer2()){
                    player2 = players.get(i);
                }
            }
        }
        holder.bind(player1.getPlayerName(), player2.getPlayerName(), player1.getPoints(), player2.getPoints(),
                Uri.parse(player1.getPictureUri()), Uri.parse(player2.getPictureUri() ));
    }

    @Override
    public int getItemCount() {
        return pairings.size();
    }

    private int getPairingPosition(int position, int pairingSize){
        return position < pairingSize-1 ? position : pairingSize-1;
    }
}

