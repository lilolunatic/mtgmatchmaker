package com.example.mtgmatchmaker.view.gamelobby;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mtgmatchmaker.R;

public class MatchRowViewHolder extends RecyclerView.ViewHolder{
    private final TextView playerName1, playerName2, pointsPlayer1, pointsPlayer2;
    private final ImageView avatar1;
    private final ImageView avatar2;

    private MatchRowViewHolder(View itemView){
        super(itemView);
        playerName1 = itemView.findViewById(R.id.namePlayer1);
        playerName2 = itemView.findViewById(R.id.namePlayer2);
        pointsPlayer1 = itemView.findViewById(R.id.pointsPlayer1);
        pointsPlayer2 = itemView.findViewById(R.id.pointsPlayer2);
        avatar1 = itemView.findViewById(R.id.avatarPlayer1);
        avatar2 = itemView.findViewById(R.id.avatarPlayer2);
    }

    public void bind(String name1, String name2, int points1, int points2, Uri player1, Uri player2) {
        playerName1.setText(name1);
        playerName2.setText(name2);
        pointsPlayer1.setText(Integer.toString(points1));
        pointsPlayer2.setText(Integer.toString(points2));
        avatar1.setImageURI(player1);
        avatar2.setImageURI(player2);
    }

    public static MatchRowViewHolder create(ViewGroup parent){
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.match_row, parent, false);
        return new MatchRowViewHolder(view);
    }
}
