package com.example.mtgmatchmaker.view.start;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.repositories.GameRepository;
import com.example.mtgmatchmaker.db.repositories.GameStatusRepository;
import com.example.mtgmatchmaker.db.pojos.Game;

import java.util.List;

public class LoadGameViewModel extends AndroidViewModel {
    private final GameRepository mRepository;
    private final GameStatusRepository changeRepo;

    public final LiveData<List<Game>> mAllGames;

    public LoadGameViewModel(Application application){
        super(application);
        MatchmakerDatabase db = MatchmakerDatabase.getDatabase(application);
        mRepository = new GameRepository(db);
        changeRepo = new GameStatusRepository(db);
        mAllGames = mRepository.getAllGames();
    }

    public void insert(Game game){mRepository.insert(game);}

    public LiveData<List<String>> loadGame(String gameName){
        return changeRepo.loadGameData(gameName);
    }
}
