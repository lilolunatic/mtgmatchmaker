package com.example.mtgmatchmaker.view.gamecreation;

import android.net.Uri;

public class PlayerHelper {
    private final String playerName;
    private String pictureUri = Uri.parse("android.resource://com.example.mtgmatchmaker/drawable/knight").toString();

    public PlayerHelper(String playerName, String pictureUri) {
        this.playerName = playerName;
        if (pictureUri != null){
            this.pictureUri = pictureUri;
        }
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getPictureUri() {
        return pictureUri;
    }

    public void setPictureUri(String pictureUri) {
        if (pictureUri != null){
            this.pictureUri = pictureUri;
        }
    }
}
