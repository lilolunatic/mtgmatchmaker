package com.example.mtgmatchmaker.view.gamelobby;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

public class GameStatusChecker<I, B> extends MediatorLiveData<Boolean> {

    private Integer roundCount = 0;
    private Integer playerCount = 0;

    public GameStatusChecker(LiveData<Integer> sourceRounds, LiveData<Integer> sourcePlayers){
        setValue(roundCount < targetRoundCount(playerCount));

        super.addSource(sourceRounds, sr ->{
            if(sr != null && playerCount > 0){
                roundCount = sr;
                setValue(roundCount >= targetRoundCount(playerCount));
            }
        });

        super.addSource(sourcePlayers, sp ->{
            if(sp != null){
                playerCount = sp;
            }
            setValue(roundCount >= targetRoundCount(playerCount));
        });
    }

    private int targetRoundCount(int playerCount){
        if(playerCount %2 == 0){
            return playerCount - 1;
        }
        return playerCount;
    }
}