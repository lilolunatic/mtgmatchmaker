package com.example.mtgmatchmaker.view.gamecreation;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.Switch;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mtgmatchmaker.R;
import com.example.mtgmatchmaker.datatransfer.GameBaseDTO;
import com.example.mtgmatchmaker.view.gamelobby.LobbyActivity;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class CreateGameActivity extends AppCompatActivity
        implements AddPlayerDialog.AddPlayerDialogListener, PlayerItemClickListener {
    private static final int SELECT_PICTURE = 200;
    private CreateGameViewModel createGameViewModel;
    private PlayerListAdapter adapter;
    private Button saveButton;
    private String gameName = "";
    private boolean isChecked;
    private int currentPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_game);

        createGameViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication())
                .create(CreateGameViewModel.class);
        RecyclerView playerList = findViewById(R.id.rv_players);
        adapter = new PlayerListAdapter(createGameViewModel.getAddedPlayers(), this);
        playerList.setAdapter(adapter);
        playerList.setLayoutManager(new LinearLayoutManager(this));
        saveButton = findViewById(R.id.gameCreationFinishButton);

        TextInputEditText gameNameInput = findViewById(R.id.gameNameTextInput);
        gameNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(!TextUtils.isEmpty(s.toString().trim())){
                    gameName = gameNameInput.getText().toString();
                    enableCreateButton();
                }
            }
        });
        Switch tournamentMode = findViewById(R.id.tournamentSelection);
        Button addButton = findViewById(R.id.addPlayerButton);
        addButton.setOnClickListener(v -> {
            AddPlayerDialog addPlayerDialog = new AddPlayerDialog();
            addPlayerDialog.show(getSupportFragmentManager(), "add player dialog");
        });

        saveButton.setOnClickListener( view -> {
            String gameName = Objects.requireNonNull(gameNameInput.getText()).toString();
            isChecked = tournamentMode.isChecked();
            createGameViewModel.createGame(gameName, isChecked);
            Intent lobbyActivity = new Intent(getApplicationContext(), LobbyActivity.class);
            GameBaseDTO gameBaseDTO = new GameBaseDTO(gameName, isChecked, createGameViewModel.getPlayerNames());
            lobbyActivity.putExtra("gameData", gameBaseDTO);
            startActivity(lobbyActivity);
            finish();
        });
    }

    private void enableCreateButton(){
        saveButton.setEnabled(createGameViewModel.getAddedPlayers().size()> 1
                && gameName.length() > 0);
    }

    @Override
    public void addPlayer(String playerName){
        createGameViewModel.addPlayerToList(new PlayerHelper(playerName, null));
        adapter.setPlayers(createGameViewModel.getAddedPlayers());
        enableCreateButton();
    }

    @Override
    public void onClickDelete(int position) {
        createGameViewModel.removePlayerFromList(position);
        enableCreateButton();
    }

    @Override
    public void onClickAddPicture(int position) {
        currentPosition = position;
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_OPEN_DOCUMENT);
        startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE); //TODO change to resource
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode ==SELECT_PICTURE){
                Uri selectedImageUri = data.getData();
                ContentResolver contentResolver = getContentResolver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    contentResolver.takePersistableUriPermission(selectedImageUri, Intent.FLAG_GRANT_READ_URI_PERMISSION );
                }
                createGameViewModel.setPictureOfPlayer(currentPosition, selectedImageUri.toString());
            }
            adapter.setPlayers(createGameViewModel.getAddedPlayers());
        }
    }
}
