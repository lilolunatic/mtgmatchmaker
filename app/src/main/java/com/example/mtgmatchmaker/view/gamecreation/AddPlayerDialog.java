package com.example.mtgmatchmaker.view.gamecreation;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import com.example.mtgmatchmaker.R;
import com.example.mtgmatchmaker.view.TextFieldHelper;

import java.util.Objects;


public class AddPlayerDialog extends AppCompatDialogFragment {
    private EditText playerNameInput;
    private AddPlayerDialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.enter_playername, null);

        playerNameInput = view.findViewById(R.id.TextInputPlayerName);

        builder.setView(view)
                .setTitle(R.string.add_player_dialog)
                .setPositiveButton(R.string.done, (dialog, which) -> {
                    String playerName = TextFieldHelper.getNameOrPlayerX(playerNameInput);
                    listener.addPlayer(playerName);
                });
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context){
        super.onAttach(context);
        listener = (AddPlayerDialogListener) context;
    }

    public interface AddPlayerDialogListener{
        void addPlayer(String playerName);
    }
}
