package com.example.mtgmatchmaker.view;

import android.widget.EditText;
import com.example.mtgmatchmaker.R;

public class TextFieldHelper {
    private static int playerNumber = 0;

    public static int getNumOrZero(EditText textField) {
        if(textField.getText().toString().isEmpty()
            || textField.getText().toString() == null){
            return 0;
        }
        return Integer.parseInt(textField.getText().toString());
    }

    public static String getNameOrPlayerX(EditText textField){
        if(textField.getText().toString().isEmpty()
        || textField.getText().toString() == null){
            String playerX = R.string.playerX + Integer.toString(playerNumber);
            playerNumber++;
            return playerX;
        }
        return textField.getText().toString();
    }
}
