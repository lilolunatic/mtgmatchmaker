package com.example.mtgmatchmaker.view.gamecreation;

import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PlayerListAdapter extends RecyclerView.Adapter<PlayerRowViewHolder> {
    private List<PlayerHelper> players;
    final private PlayerItemClickListener playerItemClickListener;

    public PlayerListAdapter(List<PlayerHelper> players, CreateGameActivity playerItemClickListener){
        this.players = players;
        this.playerItemClickListener = playerItemClickListener;
    }

    @NonNull
    @Override
    public PlayerRowViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType){
        return PlayerRowViewHolder.create(viewGroup, playerItemClickListener);
    }

    @Override
    public void onBindViewHolder(PlayerRowViewHolder holder, int position){
        PlayerHelper current = players.get(position);
        holder.bind(current);
        holder.deleteButton.setOnClickListener(v -> {
            playerItemClickListener.onClickDelete(position);
            notifyDataSetChanged();
        });

        holder.playerPicture.setOnClickListener(v -> {
            playerItemClickListener.onClickAddPicture(position);
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    public void setPlayers(List<PlayerHelper> players){
        this.players = players;
        notifyDataSetChanged();
    }
}

