package com.example.mtgmatchmaker.view.scoring;

public interface ListItemClickListener {
    void onClickListItem(int position);
}
