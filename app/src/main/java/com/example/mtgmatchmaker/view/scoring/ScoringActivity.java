package com.example.mtgmatchmaker.view.scoring;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mtgmatchmaker.R;
import com.example.mtgmatchmaker.datatransfer.PointsDialogDTO;
import com.example.mtgmatchmaker.datatransfer.ScoringDTO;
import com.example.mtgmatchmaker.view.gamelobby.matching.Pairing;

import java.util.ArrayList;
import java.util.List;

public class ScoringActivity extends AppCompatActivity implements ListItemClickListener, ScoringDialog.AddPointsDialogListener {

    private ScoringViewModel scoringViewModel;
    private ScoringListAdapter adapter;
    private final List<Pairing> pairings = new ArrayList<>();
    private ScoringDTO scoringDTO;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_points);
        scoringDTO = getIntent().getParcelableExtra("scoringData");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            scoringDTO.getPairings().forEach(p ->{
                String[] names = p.split(",");
                pairings.add(new Pairing(names[0], names[1]));
            });
        } else {
            for(int i = 0; i< scoringDTO.getPairings().size(); i++){
                String [] names = scoringDTO.getPairings().get(i).split(",");
                pairings.add(new Pairing(names[0], names[1]));
            }
        }

        RecyclerView matches = findViewById(R.id.rv_add_points);
        scoringViewModel = new ViewModelProvider(this, new ScoringViewModelFactory(this.getApplication(),
                scoringDTO.getGameName(), pairings)).get(ScoringViewModel.class);
        adapter = new ScoringListAdapter(pairings, scoringViewModel.pointsMap,  this);
        matches.setAdapter(adapter);
        matches.setLayoutManager(new LinearLayoutManager(this));

        TextView description = findViewById(R.id.description);
        description.setText(scoringDTO.isTournamentMode()? R.string.rr_description : R.string.de_description);

        Button finishButton = findViewById(R.id.confirm_points);
        finishButton.setOnClickListener(view ->{
            scoringViewModel.savePoints();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        });
    }

    @Override
    public void onClickListItem(int position) {
        ScoringDialog scoringDialog = new ScoringDialog();
        PointsDialogDTO pointsDialogDTO = new PointsDialogDTO(pairings.get(position).getPlayer1(),
                pairings.get(position).getPlayer2(), scoringDTO.isTournamentMode());
        Bundle bundle = new Bundle();
        bundle.putParcelable("pointsDialogData", pointsDialogDTO);
        scoringDialog.setArguments(bundle);
        scoringDialog.show(getSupportFragmentManager(), "add points dialog");
    }

    @Override
    public void addPoints(String player1, int points1, String player2, int points2){
        scoringViewModel.setPoints(player1, points1, player2, points2);
        adapter.setPointsMap(scoringViewModel.pointsMap);
    }
}
