package com.example.mtgmatchmaker.view.gamecreation;

import android.app.Application;
import android.os.Build;
import androidx.lifecycle.AndroidViewModel;

import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.repositories.GameRepository;
import com.example.mtgmatchmaker.db.repositories.GameStatusRepository;
import com.example.mtgmatchmaker.db.repositories.PlayerRepository;
import com.example.mtgmatchmaker.db.pojos.Game;
import com.example.mtgmatchmaker.db.pojos.Player;

import java.util.ArrayList;
import java.util.List;

public class CreateGameViewModel extends AndroidViewModel {
    private final GameRepository gameRepository;
    private final PlayerRepository playerRepository;
    private final GameStatusRepository mRepository;

    private final List<PlayerHelper> addedPlayers = new ArrayList<>();

    public CreateGameViewModel(Application application){
        super(application);
        MatchmakerDatabase db = MatchmakerDatabase.getDatabase(application);
        gameRepository = new GameRepository(db);
        playerRepository = new PlayerRepository(db);
        mRepository = new GameStatusRepository(db);
    }

    public void addPlayerToList(PlayerHelper player){
        addedPlayers.add(player);
    }

    public void removePlayerFromList(int position){
        addedPlayers.remove(position);
    }

    public void setPictureOfPlayer(int position, String pictureUri){
        addedPlayers.get(position).setPictureUri(pictureUri);
    }

    public List<PlayerHelper> getAddedPlayers(){
        return addedPlayers;
    }

    public String[] getPlayerNames(){
        String[] names = new String[addedPlayers.size()];
        for(int i = 0; i< addedPlayers.size(); i++){
            names[i] = addedPlayers.get(i).getPlayerName();
        }
        return names;
    }

    public void insert(Player player){playerRepository.insert(player);}

    public void createGame(String gameName, boolean isRoundRobin) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            addedPlayers.forEach(p -> playerRepository.insert(new Player(p.getPlayerName())));
        } else {
            for(int i = 0; i < addedPlayers.size(); i++){
                playerRepository.insert(new Player(addedPlayers.get(i).getPlayerName()));
            }
        }
        gameRepository.insert(new Game(gameName, isRoundRobin));
        mRepository.insertPlayersOfGame(addedPlayers, gameName);
    }
}
