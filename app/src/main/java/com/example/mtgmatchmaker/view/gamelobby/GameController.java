package com.example.mtgmatchmaker.view.gamelobby;

public class GameController {
    private boolean isRoundRobin;
    private boolean isCurrentlyMatching = false;
    private boolean isScoringAllowed = false;
    private boolean isDisplayingRanking = false;

    public GameController(boolean isRoundRobin) {
        this.isRoundRobin = isRoundRobin;
    }

    public boolean isRoundRobin() {
        return isRoundRobin;
    }

    public void setRoundRobin(boolean roundRobin) {
        isRoundRobin = roundRobin;
    }

    public boolean isCurrentlyMatching() {
        return isCurrentlyMatching;
    }

    public void setCurrentlyMatching(boolean currentlyMatching) {
        isCurrentlyMatching = currentlyMatching;
    }

    public void setScoringAllowed(boolean scoringAllowed) {
        isScoringAllowed = scoringAllowed;
    }

    public void matchingOngoing(){
        isCurrentlyMatching = true;
        isScoringAllowed = false;
    }

    public void finishedOrScoring(){
        isCurrentlyMatching = false;
        isScoringAllowed = true;
    }

    public boolean notFinished (Object isFinished){
        return !isCurrentlyMatching && isScoringAllowed && isFinished.equals(true);
    }

    public boolean isFinished(Object isFinished){
        if(!isDisplayingRanking){
            isDisplayingRanking = true;
            return isFinished.equals(true) && ! isCurrentlyMatching;
        }
        return false;
    }

    public boolean isDoubleEliminationFinished(int count){
        return count<2 && !isCurrentlyMatching;
    }

    public boolean isDoubleEliminationOngoing(int count) {
        return count>=2 && !isCurrentlyMatching && isScoringAllowed;
    }
}
