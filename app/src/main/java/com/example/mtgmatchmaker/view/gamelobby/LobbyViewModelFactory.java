package com.example.mtgmatchmaker.view.gamelobby;

import android.app.Application;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class LobbyViewModelFactory implements ViewModelProvider.Factory {
    private final Application mApplication;
    private final String mGameName;

    public LobbyViewModelFactory(Application application, String gameName){
        mApplication = application;
        mGameName = gameName;
    }

    @NonNull
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new LobbyViewModel(mApplication, mGameName);
    }
}
