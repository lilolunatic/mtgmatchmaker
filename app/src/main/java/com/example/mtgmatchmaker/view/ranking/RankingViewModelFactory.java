package com.example.mtgmatchmaker.view.ranking;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class RankingViewModelFactory implements ViewModelProvider.Factory {
    private final Application mApplication;
    private final String mGameName;
    private final Boolean mIsRoundRobin;

    public RankingViewModelFactory(Application application, String gameName, Boolean isRoundRobin){
        mApplication = application;
        mGameName = gameName;
        mIsRoundRobin = isRoundRobin;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new RankingViewModel(mApplication, mGameName, mIsRoundRobin);
    }
}
