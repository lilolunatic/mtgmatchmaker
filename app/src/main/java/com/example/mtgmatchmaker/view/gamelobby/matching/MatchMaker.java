package com.example.mtgmatchmaker.view.gamelobby.matching;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;

import java.util.ArrayList;
import java.util.List;

public class MatchMaker {
    private final List<String> row1 = new ArrayList<>();
    private final List<String> row2 = new ArrayList<>();
    private final boolean isRoundRobin;
    private int roundCount;
    private boolean even;

    public MatchMaker(String[] players, boolean isRoundRobin, int roundCount){
        this.isRoundRobin = isRoundRobin;
        this.roundCount = roundCount;
        initializeMatching(players);
        resumeGame();
    }

    private void prepareNextRound(){
        if(row1.size()>1) {
            row1.add(even? 1: 0, row2.get(row2.size() - 1));
            row2.remove(row2.size() - 1);
            row2.add(0, row1.get(row1.size() - 1));
            row1.remove(row1.size() - 1);
        }
    }

    private void initializeMatching(String[] players){
        for(int i = 0; i < players.length; i++){
            if(i % 2 == 0 ){
                row1.add(players[i]);
            } else {
              row2.add(players[i]);
            }
        }
        even = row1.size() == row2.size();
    }

    private void resumeGame(){
        if(roundCount > 0 && isRoundRobin){
            for(int i = 0; i<roundCount; i++){
                prepareNextRound();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<Pairing> match(List<GamePlayerCrossRef> players){
        if(isRoundRobin){
            return matchAllVsAll();
        }
        return matchDoubleElimination(players);
    }

    private List<Pairing> matchAllVsAll(){
        prepareNextRound();
        int pairingSize = 0;
        pairingSize = row1.size() < row2.size() ? row1.size() : row2.size();

        List<Pairing> pairings = new ArrayList<>();

        for(int i = 0; i< pairingSize ; i++){
            pairings.add(new Pairing(row1.get(i), row2.get(row2.size()-(i+1))));
        }
        return pairings;
    }

    private List<Pairing> matchDoubleElimination(List<GamePlayerCrossRef> players){
        if(roundCount == 0){
            roundCount++;
            return matchAllVsAll();
        }
        DoubleEliminationManager manager = new DoubleEliminationManager();
        return manager.matchDoubleElimination(players);
    }
}
