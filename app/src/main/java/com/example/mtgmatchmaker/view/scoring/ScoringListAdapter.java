package com.example.mtgmatchmaker.view.scoring;

import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mtgmatchmaker.view.gamelobby.matching.Pairing;

import java.util.HashMap;
import java.util.List;

public class ScoringListAdapter extends RecyclerView.Adapter<ScoringViewHolder>{
    final private ListItemClickListener mOnClickListener;
    private HashMap<String, MutableLiveData<Integer>> pointsMap;
    final private List<Pairing> pairings;

    public ScoringListAdapter(List<Pairing> pairings, HashMap<String, MutableLiveData<Integer>> pointsMap, ListItemClickListener onClickListener){
        this.pairings = pairings;
        this.mOnClickListener = onClickListener;
        this.pointsMap = pointsMap;
    }

    @NonNull
    @Override
    public ScoringViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        return ScoringViewHolder.create(parent, mOnClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ScoringViewHolder holder, int position){
        if(position < pairings.size()){
            String player1 = pairings.get(position).getPlayer1();
            String player2 = pairings.get(position).getPlayer2();
            holder.bind(player1, player2, pointsMap.get(player1).getValue(), pointsMap.get(player2).getValue());
        }
    }

    @Override
    public int getItemCount() {
        return pairings.size();
    }

    public void setPointsMap(HashMap<String, MutableLiveData<Integer>> pointsMap){
        this.pointsMap = pointsMap;
        notifyDataSetChanged();
    }
}
