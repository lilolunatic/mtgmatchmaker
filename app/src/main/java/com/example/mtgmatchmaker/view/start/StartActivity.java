package com.example.mtgmatchmaker.view.start;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mtgmatchmaker.datatransfer.GameBaseDTO;
import com.example.mtgmatchmaker.R;
import com.example.mtgmatchmaker.db.pojos.Game;
import com.example.mtgmatchmaker.view.gamecreation.CreateGameActivity;
import com.example.mtgmatchmaker.view.gamelobby.LobbyActivity;
import com.example.mtgmatchmaker.view.scoring.ListItemClickListener;

import java.util.List;

public class StartActivity extends AppCompatActivity implements ListItemClickListener {
    private LoadGameViewModel loadGameViewModel;
    private List<Game> games = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadGameViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication())
                .create(LoadGameViewModel.class);
        RecyclerView gameView = findViewById(R.id.recyclerview);
        final GameListAdapter adapter = new GameListAdapter(new GameListAdapter.GameDiff(),this);
        loadGameViewModel.mAllGames.observe(this, games ->{
            this.games = games;
            adapter.submitList(games);
        });
        Button newGameButton = findViewById(R.id.newGameButton);
        newGameButton.setOnClickListener(v -> {
                Intent createGame = new Intent(getApplicationContext(), CreateGameActivity.class);
                startActivity(createGame);
        });

        gameView.setAdapter(adapter);
        gameView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onClickListItem(int position) {
        String gameName = games.get(position).getGameName();
        Intent lobbyActivity = new Intent(getApplicationContext(), LobbyActivity.class);
        Bundle lobbyBundle = new Bundle();

        loadGameViewModel.loadGame(gameName).observe(this, players ->{
            GameBaseDTO gameData = new GameBaseDTO(gameName, players.toArray(new String[0]));
            lobbyActivity.putExtra("gameData", gameData);
            startActivity(lobbyActivity);
        });
    }
}