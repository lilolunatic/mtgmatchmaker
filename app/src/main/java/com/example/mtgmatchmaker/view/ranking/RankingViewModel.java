package com.example.mtgmatchmaker.view.ranking;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.repositories.GameStatusRepository;
import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;

import java.util.List;

public class RankingViewModel extends AndroidViewModel {
    private final GameStatusRepository mRepository;
    private final String mGameName;
    private final Boolean mIsRoundRobin;
    private LiveData<List<GamePlayerCrossRef>> mRanking;

    public RankingViewModel(Application application, String gameName, Boolean isRoundRobin) {
        super(application);
        MatchmakerDatabase db = MatchmakerDatabase.getDatabase(application);
        mRepository = new GameStatusRepository(db);
        mGameName = gameName;
        mIsRoundRobin = isRoundRobin;
        getRanking();
    }

    private void getRanking(){
        if(mIsRoundRobin){
            mRanking = mRepository.getRoundRobinRanking(mGameName);
        } else {
            mRanking = mRepository.getDoubleEliminationRanking(mGameName);
        }
    }

    public LiveData<List<GamePlayerCrossRef>> getRankingList(){
        return mRanking;
    }
}
