package com.example.mtgmatchmaker.view.start;

import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import com.example.mtgmatchmaker.db.pojos.Game;
import com.example.mtgmatchmaker.view.scoring.ListItemClickListener;

public class GameListAdapter extends ListAdapter<Game, GameViewHolder>{
    final private ListItemClickListener listItemClickListener;

    public GameListAdapter(@NonNull DiffUtil.ItemCallback<Game> diffCallback, StartActivity listItemClickListener){
        super(diffCallback);
        this.listItemClickListener = listItemClickListener;
    }

    @NonNull
    @Override
    public GameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType ){
        return GameViewHolder.create(parent, listItemClickListener);
    }

    @Override
    public void onBindViewHolder(GameViewHolder holder, int position){
        Game current = getItem(position);
        holder.bind(current.getGameName());
    }

    public static class GameDiff extends DiffUtil.ItemCallback<Game> {
        @Override
        public boolean areItemsTheSame(@NonNull Game oldItem, @NonNull Game newItem){
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull Game oldItem, @NonNull Game newItem){
            return oldItem.getGameName().equals(newItem.getGameName());
        }
    }
}
