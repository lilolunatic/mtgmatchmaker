package com.example.mtgmatchmaker.view.gamelobby.matching;

import android.os.Build;

import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DoubleEliminationManager {

    public List<Pairing> matchDoubleElimination(List<GamePlayerCrossRef> players){
        List<Pairing> pairings = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            players = players.stream().filter(p -> p.getPoints() < 2).collect(Collectors.toList());
        } else {
            for(int i = 0; i<players.size(); i++){
                if(players.get(i).getPoints() >= 2){
                    players.remove(i);
                }
            }
        }

        Collections.sort(players);
        for(int i = 0; i < players.size(); i+=2 ){
            if(i+1 >= players.size()){
                break;
            }
            pairings.add(new Pairing(players.get(i).getPlayerName(), players.get(i+1).getPlayerName()));
        }
        return pairings;
    }
}
