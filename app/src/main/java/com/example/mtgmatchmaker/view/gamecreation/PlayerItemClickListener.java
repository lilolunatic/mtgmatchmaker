package com.example.mtgmatchmaker.view.gamecreation;

public interface PlayerItemClickListener {
    void onClickDelete(int position);
    void onClickAddPicture(int position);
}
