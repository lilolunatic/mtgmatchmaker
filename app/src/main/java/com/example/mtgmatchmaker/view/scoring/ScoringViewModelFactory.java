package com.example.mtgmatchmaker.view.scoring;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.mtgmatchmaker.view.gamelobby.matching.Pairing;

import java.util.List;

public class ScoringViewModelFactory implements ViewModelProvider.Factory {
    private final Application mApplication;
    private final String mGameName;
    private final List<Pairing> mPairings;

    public ScoringViewModelFactory(Application application, String gameName, List<Pairing> pairings){
        mApplication = application;
        mGameName = gameName;
        mPairings = pairings;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ScoringViewModel(mApplication, mGameName, mPairings);
    }
}