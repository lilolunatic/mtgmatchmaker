package com.example.mtgmatchmaker.view.gamelobby;

import android.app.Application;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.repositories.GameRepository;
import com.example.mtgmatchmaker.db.repositories.GameStatusRepository;
import com.example.mtgmatchmaker.db.pojos.Game;
import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;

import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.N)
public class LobbyViewModel extends AndroidViewModel {

    private final GameRepository mRepository;
    private final GameStatusRepository gameStatusRepository;
    private LiveData<List<GamePlayerCrossRef>> mGameStatus;
    private final String mGameName;
    private final LiveData<Integer> mRoundCount;
    private final LiveData<Integer> mPlayerCount;

    public LobbyViewModel(Application application, String gameName){
        super(application);
        mGameName = gameName;
        MatchmakerDatabase db = MatchmakerDatabase.getDatabase(application);
        mRepository = new GameRepository(db);
        gameStatusRepository = new GameStatusRepository(db);
        mGameStatus = gameStatusRepository.getGameStatus(gameName);
        mRoundCount = mRepository.getRoundCount(gameName);
        mPlayerCount = gameStatusRepository.getPlayerCount(gameName);
    }

    public void increaseRoundCount(){
        mRepository.increaseRoundCount(mGameName);
    }

    public LiveData<Integer> getRoundCount() {return mRoundCount;}

    public LiveData<Integer> getPlayerCount() {return mPlayerCount;}

    public LiveData<Integer> getDoubleEliminationPlayerCount(){
        return gameStatusRepository.getDoubleEliminationPlayerCount(mGameName);
    }

    public LiveData<List<GamePlayerCrossRef>> getGameStatus() {
        return mGameStatus;
    }

    public LiveData<Game> getGame(String gameName) {return mRepository.getGame(gameName);}
}
