package com.example.mtgmatchmaker.view.gamelobby;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mtgmatchmaker.R;
import com.example.mtgmatchmaker.datatransfer.GameBaseDTO;
import com.example.mtgmatchmaker.datatransfer.ScoringDTO;
import com.example.mtgmatchmaker.db.pojos.Game;
import com.example.mtgmatchmaker.view.gamelobby.matching.MatchMaker;
import com.example.mtgmatchmaker.view.gamelobby.matching.Pairing;
import com.example.mtgmatchmaker.view.ranking.RankingActivity;
import com.example.mtgmatchmaker.view.scoring.ScoringActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LobbyActivity extends AppCompatActivity {
    private LobbyViewModel mLobbyViewModel;
    private MatchListAdapter adapter;
    private RecyclerView matches;
    private MatchMaker matchMaker;
    private List<Pairing> pairings;
    private GameController controller;
    private GameBaseDTO gameBaseDTO;
    private boolean isRoundRobin;
    private int roundCount;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);
        gameBaseDTO = getIntent().getParcelableExtra("gameData");
        TextView gameNameLobby = findViewById(R.id.gameNameLobby);
        gameNameLobby.setText(gameBaseDTO.getGameName());

        mLobbyViewModel = new ViewModelProvider(this, new LobbyViewModelFactory(this.getApplication(), gameBaseDTO.getGameName()))
                .get(LobbyViewModel.class);
        LiveData<Game> game = mLobbyViewModel.getGame(gameBaseDTO.getGameName());
        game.observe(this, g -> {
            controller = new GameController(g.isRoundRobin());
            isRoundRobin = g.isRoundRobin();
            roundCount = g.getRoundCount();
        });
        matches = findViewById(R.id.matchRecyclerView);
        mLobbyViewModel.getGameStatus().observe(this, p -> {
            matchMaker = new MatchMaker(gameBaseDTO.getPlayers(), controller.isRoundRobin(), roundCount);
            pairings = matchMaker.match(p);
            adapter = new MatchListAdapter(pairings, p);
            matches.setAdapter(adapter);
        });
        matches.setLayoutManager(new LinearLayoutManager(this));

        Button matchButton = findViewById(R.id.matchButton);
        matchButton.setOnClickListener(view -> {
            controller.setScoringAllowed(true);
            scoreAndNextRound();
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void scoreAndNextRound(){
        ArrayList<String> pairingsList = pairings.stream().map(p -> p.getPlayer1() + "," + p.getPlayer2())
                .collect(Collectors.toCollection(ArrayList::new));
        ScoringDTO scoringDTO = new ScoringDTO(gameBaseDTO.getGameName(), isRoundRobin, pairingsList);
        Intent addPointsActivity = new Intent(getApplicationContext(), ScoringActivity.class);
        addPointsActivity.putExtra("scoringData", scoringDTO);
        startActivityForResult(addPointsActivity, 1);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        controller.setScoringAllowed(false);
        if (requestCode == 1) {
            mLobbyViewModel.getGameStatus().observe(this, p -> {
                pairings = matchMaker.match(p);
                adapter = new MatchListAdapter(pairings, p);
                matches.setAdapter(adapter);
            });
            mLobbyViewModel.increaseRoundCount();
            checkGameStatus();
        }
        controller.setCurrentlyMatching(false);
    }

    private void checkGameStatus(){
        if(controller.isRoundRobin()){
            GameStatusChecker gameStatusChecker = new GameStatusChecker(mLobbyViewModel.getRoundCount(), mLobbyViewModel.getPlayerCount());
            gameStatusChecker.observe(this, isFinished -> {
                if(controller.isFinished(isFinished)){
                    displayRanking();
                    controller.matchingOngoing();
                }else if( controller.notFinished(isFinished)) {
                    scoreAndNextRound();
                    controller.matchingOngoing();
                }
            });
        }else {
            mLobbyViewModel.getDoubleEliminationPlayerCount().observe(this, count ->{
                if(controller.isDoubleEliminationFinished(count)){
                    displayRanking();
                    controller.finishedOrScoring();
                }
                else if(controller.isDoubleEliminationOngoing(count)){
                    scoreAndNextRound();
                    controller.finishedOrScoring();
                }
            });
        }
    }

    private void displayRanking(){
        Intent rankingActivity = new Intent(getApplicationContext(), RankingActivity.class);
        GameBaseDTO rankingData = new GameBaseDTO(gameBaseDTO.getGameName(), isRoundRobin);
        rankingActivity.putExtra("rankingData", rankingData);
        startActivity(rankingActivity);
        finish();
    }
}
