package com.example.mtgmatchmaker.view.ranking;

import android.net.Uri;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;

public class RankingAdapter extends ListAdapter<GamePlayerCrossRef, RankingViewHolder> {
    public RankingAdapter(@NonNull DiffUtil.ItemCallback<GamePlayerCrossRef> diffCalback){
        super(diffCalback);
    }

    @NonNull
    @Override
    public RankingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        return RankingViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(RankingViewHolder holder, int position){
        GamePlayerCrossRef current = getItem(position);
        holder.bind(current.getPlayerName(), Uri.parse(current.getPictureUri()), current.getPoints());
    }

    public static class GPCRDiff extends DiffUtil.ItemCallback<GamePlayerCrossRef>{
        @Override
        public boolean areItemsTheSame(@NonNull GamePlayerCrossRef oldItem, @NonNull GamePlayerCrossRef newItem){
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull GamePlayerCrossRef oldItem, @NonNull GamePlayerCrossRef newItem) {
            return oldItem.getPictureUri().equals(newItem.getPictureUri())
                    && oldItem.getPoints() == newItem.getPoints()
                    && oldItem.getPlayerName().equals(newItem.getPlayerName())
                    && oldItem.getTimesMatched() == newItem.getTimesMatched();
        }
    }
}
