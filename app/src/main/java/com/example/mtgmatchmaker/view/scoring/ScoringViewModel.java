package com.example.mtgmatchmaker.view.scoring;

import android.app.Application;
import android.os.Build;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.repositories.GameStatusRepository;
import com.example.mtgmatchmaker.view.gamelobby.matching.Pairing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScoringViewModel extends AndroidViewModel {
    private final GameStatusRepository mRepository;
    private final String mGameName;
    public final HashMap<String, MutableLiveData<Integer>> pointsMap;

    public ScoringViewModel(Application application, String gameName, List<Pairing> pairings){
        super(application);
        MatchmakerDatabase db = MatchmakerDatabase.getDatabase(application);
        mRepository = new GameStatusRepository(db);
        mGameName = gameName;
        pointsMap  = new HashMap<String, MutableLiveData<Integer>>(){{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                pairings.forEach(p ->{
                    put(p.getPlayer1(), new MutableLiveData<>(0));
                    put(p.getPlayer2(), new MutableLiveData<>(0));
                });
            } else {
                for(int i = 0; i<pairings.size(); i++){
                    put(pairings.get(i).getPlayer1(), new MutableLiveData<>(0));
                    put(pairings.get(i).getPlayer2(), new MutableLiveData<>(0));
                }
            }
        }};

    }

    public void setPoints(String player1, int points1, String player2, int points2){
        pointsMap.get(player1).postValue(points1);
        pointsMap.get(player2).postValue(points2);
    }

    public void savePoints(){
        for(Map.Entry<String, MutableLiveData<Integer>> entry : pointsMap.entrySet()){
            mRepository.addPoints(mGameName, entry.getKey(), entry.getValue().getValue());
            mRepository.increaseTimesMatched(mGameName, entry.getKey());
        }
    }
}
