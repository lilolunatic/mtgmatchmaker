package com.example.mtgmatchmaker.view.ranking;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mtgmatchmaker.R;
import com.example.mtgmatchmaker.datatransfer.GameBaseDTO;

public class RankingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);
        GameBaseDTO rankingData = getIntent().getParcelableExtra("rankingData");

        RankingViewModel mRankingViewModel = new ViewModelProvider(this, new RankingViewModelFactory(
                this.getApplication(), rankingData.getGameName(), rankingData.getTournamentMode()))
                .get(RankingViewModel.class);

        RecyclerView ranking = findViewById(R.id.rvPlayerRanking);
        RankingAdapter adapter = new RankingAdapter(new RankingAdapter.GPCRDiff());
        mRankingViewModel.getRankingList().observe(this, adapter::submitList);
        ranking.setAdapter(adapter);
        ranking.setLayoutManager(new LinearLayoutManager(this));
    }
}
