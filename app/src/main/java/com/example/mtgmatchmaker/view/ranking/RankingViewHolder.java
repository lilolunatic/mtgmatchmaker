package com.example.mtgmatchmaker.view.ranking;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mtgmatchmaker.R;

public class RankingViewHolder extends RecyclerView.ViewHolder {
    private final TextView playerName, playerPoints;
    private final ImageView avatar;

    private RankingViewHolder(View itemView){
        super(itemView);
        playerName = itemView.findViewById(R.id.namePlayer);
        playerPoints = itemView.findViewById(R.id.pointsPlayer);
        avatar = itemView.findViewById(R.id.avatar);
    }

    public void bind(String playerName, Uri avatar, int points){
        this.playerName.setText(playerName);
        this.avatar.setImageURI(avatar);
        playerPoints.setText(Integer.toString(points));
    }

    public static RankingViewHolder create(ViewGroup parent){
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ranking_row, parent, false);
        return new RankingViewHolder(view);
    }
}
