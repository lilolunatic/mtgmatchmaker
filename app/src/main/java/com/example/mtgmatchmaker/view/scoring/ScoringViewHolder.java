package com.example.mtgmatchmaker.view.scoring;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mtgmatchmaker.R;

public class ScoringViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
    private final TextView playerName1, playerName2, pointsPlayer1, pointsPlayer2;
    private final ListItemClickListener listItemClickListener;


    private ScoringViewHolder(View itemView, ListItemClickListener listItemClickListener){
        super(itemView);
        itemView.setOnClickListener(this);
        this.listItemClickListener = listItemClickListener;
        playerName1 = itemView.findViewById(R.id.namePlayer1);
        playerName2 = itemView.findViewById(R.id.namePlayer2);
        pointsPlayer1 = itemView.findViewById(R.id.pointsPlayer1);
        pointsPlayer2 = itemView.findViewById(R.id.pointsPlayer2);
    }

    public void bind(String name1, String name2, int points1, int points2) {
        playerName1.setText(name1);
        playerName2.setText(name2);
        pointsPlayer1.setText(Integer.toString(points1));
        pointsPlayer2.setText(Integer.toString(points2));
    }

    public static ScoringViewHolder create(ViewGroup parent, ListItemClickListener listItemClickListener){
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.points_row, parent, false);
        return new ScoringViewHolder(view, listItemClickListener);
    }

    @Override
    public void onClick(View v) {
        int position = getAdapterPosition();
        listItemClickListener.onClickListItem(position);
    }
}
