package com.example.mtgmatchmaker.view.scoring;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;
import com.example.mtgmatchmaker.R;
import com.example.mtgmatchmaker.datatransfer.PointsDialogDTO;
import com.example.mtgmatchmaker.view.TextFieldHelper;

import java.util.Objects;

public class ScoringDialog extends AppCompatDialogFragment {
    private EditText pointsPlayer1;
    private EditText pointsPlayer2;
    private AddPointsDialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.add_points, null);

        PointsDialogDTO pointsDialogDTO = getArguments().getParcelable("pointsDialogData");

        pointsPlayer1 = view.findViewById(R.id.pointsPlayer1Input);
        pointsPlayer2 = view.findViewById(R.id.pointsPlayer2Input);

        builder.setView(view)
                .setTitle(pointsDialogDTO.isTournamentMode()? R.string.add_points : R.string.lose_life)
                .setCancelable(true)
                .setPositiveButton(R.string.done, (dialog, which) -> {
                    int points1 = TextFieldHelper.getNumOrZero(pointsPlayer1);
                    int points2 = TextFieldHelper.getNumOrZero(pointsPlayer2);
                    listener.addPoints(pointsDialogDTO.getPlayer1(), points1, pointsDialogDTO.getPlayer2(), points2);
                });
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context){
        super.onAttach(context);
        listener = (AddPointsDialogListener) context;
    }

    public interface AddPointsDialogListener{
        void addPoints(String player1, int points1, String player2, int points2);
    }
}
