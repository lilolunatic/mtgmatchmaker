package com.example.mtgmatchmaker.view.gamecreation;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mtgmatchmaker.R;

public class PlayerRowViewHolder extends RecyclerView.ViewHolder implements PlayerItemClickListener {
    private final TextView playerView;
    public final Button deleteButton;
    public final ImageView playerPicture;
    private final PlayerItemClickListener playerItemClickListener;

    private PlayerRowViewHolder(View itemView, PlayerItemClickListener playerItemClickListener){
        super(itemView);
        playerView = itemView.findViewById(R.id.playerName);
        playerPicture = itemView.findViewById(R.id.addPlayerPicture);
        this.playerItemClickListener = playerItemClickListener;
        deleteButton = itemView.findViewById(R.id.deletePlayer);
    }

    public void bind(PlayerHelper player) {
        playerView.setText(player.getPlayerName());
        playerPicture.setImageURI(Uri.parse(player.getPictureUri()));
    }

    public static PlayerRowViewHolder create(ViewGroup parent, PlayerItemClickListener playerItemClickListener){
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.player_row, parent, false);
        return new PlayerRowViewHolder(view, playerItemClickListener);
    }

    @Override
    public void onClickDelete(int position) {
        playerItemClickListener.onClickDelete(position);
    }

    @Override
    public void onClickAddPicture(int position) {
        playerItemClickListener.onClickAddPicture(position);
    }
}
