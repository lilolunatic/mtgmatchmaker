package com.example.mtgmatchmaker.datatransfer;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ScoringDTO implements Parcelable {
    private String gameName;
    private boolean tournamentMode;

    public String getGameName() {
        return gameName;
    }
    public boolean isTournamentMode() {
        return tournamentMode;
    }
    public List<String> getPairings() {
        return pairings;
    }

    private List<String> pairings;

    public ScoringDTO(String gameName, boolean tournamentMode, List<String> pairings) {
        this.gameName = gameName;
        this.tournamentMode = tournamentMode;
        this.pairings = pairings;
    }

    protected ScoringDTO(Parcel in) {
        gameName = in.readString();
        tournamentMode = in.readByte() != 0;
        pairings = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(gameName);
        dest.writeByte((byte) (tournamentMode ? 1 : 0));
        dest.writeStringList(pairings);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ScoringDTO> CREATOR = new Creator<ScoringDTO>() {
        @Override
        public ScoringDTO createFromParcel(Parcel in) {
            return new ScoringDTO(in);
        }

        @Override
        public ScoringDTO[] newArray(int size) {
            return new ScoringDTO[size];
        }
    };
}
