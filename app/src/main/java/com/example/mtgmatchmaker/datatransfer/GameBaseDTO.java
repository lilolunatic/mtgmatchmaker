package com.example.mtgmatchmaker.datatransfer;

import android.os.Parcel;
import android.os.Parcelable;

public class GameBaseDTO implements Parcelable {
    private boolean tournamentMode;
    private String gameName;
    private String[] players;

    public GameBaseDTO(String gameName, String[] players){
        this.gameName = gameName;
        this.players = players;
    }

    public GameBaseDTO(String gameName, boolean tournamentMode){
        this.gameName = gameName;
        this.tournamentMode = tournamentMode;
    }

    public GameBaseDTO(String gameName, boolean tournamentMode, String[] players){
        this.gameName = gameName;
        this.tournamentMode = tournamentMode;
        this.players = players;
    }

    public boolean getTournamentMode(){
        return tournamentMode;
    }
    public String getGameName(){return gameName;}
    public String[] getPlayers(){return players;}

    protected GameBaseDTO(Parcel in) {
        tournamentMode = in.readByte() != 0;
        gameName = in.readString();
        players = in.createStringArray();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (tournamentMode ? 1 : 0));
        dest.writeString(gameName);
        dest.writeStringArray(players);
    }

    public static final Creator<GameBaseDTO> CREATOR = new Creator<GameBaseDTO>() {
        @Override
        public GameBaseDTO createFromParcel(Parcel in) {
            return new GameBaseDTO(in);
        }

        @Override
        public GameBaseDTO[] newArray(int size) {
            return new GameBaseDTO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
}
