package com.example.mtgmatchmaker.datatransfer;

import android.os.Parcel;
import android.os.Parcelable;

public class PointsDialogDTO implements Parcelable {
    private String player1;

    public String getPlayer1() {
        return player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public boolean isTournamentMode() {
        return tournamentMode;
    }

    public PointsDialogDTO(String player1, String player2, boolean tournamentMode) {
        this.player1 = player1;
        this.player2 = player2;
        this.tournamentMode = tournamentMode;
    }

    private String player2;
    private boolean tournamentMode;

    protected PointsDialogDTO(Parcel in) {
        player1 = in.readString();
        player2 = in.readString();
        tournamentMode = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(player1);
        dest.writeString(player2);
        dest.writeByte((byte) (tournamentMode ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PointsDialogDTO> CREATOR = new Creator<PointsDialogDTO>() {
        @Override
        public PointsDialogDTO createFromParcel(Parcel in) {
            return new PointsDialogDTO(in);
        }

        @Override
        public PointsDialogDTO[] newArray(int size) {
            return new PointsDialogDTO[size];
        }
    };
}
