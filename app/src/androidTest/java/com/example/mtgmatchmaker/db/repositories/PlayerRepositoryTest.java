package com.example.mtgmatchmaker.db.repositories;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mtgmatchmaker.LiveDataTestHelper;
import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.daos.PlayerDao;
import com.example.mtgmatchmaker.db.pojos.Player;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PlayerRepositoryTest {
    private MatchmakerDatabase db;
    private PlayerDao playerDaoMock;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
        db = Mockito.mock(MatchmakerDatabase.class);
        playerDaoMock = Mockito.mock(PlayerDao.class);
        when(db.playerDao()).thenReturn(playerDaoMock);
    }

    @Test
    public void insert() {
        Player player = new Player("Name");
        PlayerRepository playerRepository = new PlayerRepository(db);
        playerRepository.insert(player);
        verify(playerDaoMock).insert(player);
    }

    @Test
    public void getAllPlayers() throws InterruptedException {
        Player player1 = new Player("Player1");
        Player player2 = new Player("Player2");
        List<Player> players = List.of(player1, player2);
        MutableLiveData<List<Player>> playersList = new MutableLiveData<>(players);

        when(playerDaoMock.getPlayers()).thenReturn(playersList);
        PlayerRepository playerRepository = new PlayerRepository(db);

        LiveData<List<Player>> fetchedPlayers = playerRepository.getAllPlayers();
        verify(playerDaoMock, times(1)).getPlayers();
        Assert.assertEquals("Player1", LiveDataTestHelper.getLiveDataValue(fetchedPlayers).get(0).getPlayerName());
    }
}