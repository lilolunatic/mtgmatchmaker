package com.example.mtgmatchmaker.db.daos;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.mtgmatchmaker.LiveDataTestHelper;
import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.pojos.Player;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

@RunWith(AndroidJUnit4.class)
public class PlayerDaoTest {
    private PlayerDao playerDao;
    private MatchmakerDatabase db;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void createDb(){
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, MatchmakerDatabase.class).build();
        playerDao = db.playerDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void insertGetPlayerTest() throws Exception {
        Player player = new Player("insert");
        playerDao.insert(player);
        LiveData<List<Player>> dbPlayers = playerDao.getPlayers();
        Player foundPlayer = LiveDataTestHelper.getLiveDataValue(dbPlayers).get(0);
        Assert.assertEquals("insert", foundPlayer.getPlayerName());
    }
}