package com.example.mtgmatchmaker.db.repositories;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mtgmatchmaker.LiveDataTestHelper;
import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.daos.GameWithPlayerDao;
import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;
import com.example.mtgmatchmaker.view.gamecreation.PlayerHelper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GameStatusRepositoryTest {
    private MatchmakerDatabase db;
    private GameWithPlayerDao gamePlayerDaoMock;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
        db = Mockito.mock(MatchmakerDatabase.class);
        gamePlayerDaoMock = Mockito.mock(GameWithPlayerDao.class);
        when(db.gamesWithPlayersDao()).thenReturn(gamePlayerDaoMock);
    }

    private List<GamePlayerCrossRef> createGameStatusList(){
        GamePlayerCrossRef player1 = new GamePlayerCrossRef("player1", "game1", "pictureUri1");
        GamePlayerCrossRef player2 = new GamePlayerCrossRef("player2", "game1", "pictureUri2");
        GamePlayerCrossRef player3 = new GamePlayerCrossRef("player3", "game2", "pictureUri2");
        GamePlayerCrossRef player4 = new GamePlayerCrossRef("player4", "game2", "pictureUri2");
        GamePlayerCrossRef player5 = new GamePlayerCrossRef("player5", "game2", "pictureUri2");
        List<GamePlayerCrossRef> gameStatus = List.of(player1, player2, player3, player4, player5);
        return gameStatus;
    }

    private LiveData<List<GamePlayerCrossRef>> createLiveDataStatus(){
        List<GamePlayerCrossRef> gameStatus = createGameStatusList();
        LiveData<List<GamePlayerCrossRef>> gameStatusLd = new MutableLiveData<>(gameStatus);
        return gameStatusLd;
    }

    private void assertTestData(LiveData<List<GamePlayerCrossRef>> fetchedData) throws InterruptedException {
        Assert.assertEquals(5, LiveDataTestHelper.getLiveDataValue(fetchedData).size());
        Assert.assertEquals("player1", LiveDataTestHelper.getLiveDataValue(fetchedData)
                .get(0).getPlayerName());
        Assert.assertEquals("player2", LiveDataTestHelper.getLiveDataValue(fetchedData)
                .get(1).getPlayerName());
        Assert.assertEquals("player3", LiveDataTestHelper.getLiveDataValue(fetchedData)
                .get(2).getPlayerName());
        Assert.assertEquals("player4", LiveDataTestHelper.getLiveDataValue(fetchedData)
                .get(3).getPlayerName());
        Assert.assertEquals("player5", LiveDataTestHelper.getLiveDataValue(fetchedData)
                .get(4).getPlayerName());
    }

    @Test
    public void getPlayerCountTest() throws InterruptedException{
        LiveData<Integer> playerCount = new MutableLiveData<>(5);
        when(gamePlayerDaoMock.getPlayerCount("Game")).thenReturn(playerCount);
        GameStatusRepository gameStatusRepository = new GameStatusRepository(db);

        Assert.assertEquals(5, LiveDataTestHelper.getLiveDataValue(
                gameStatusRepository.getPlayerCount("Game")).intValue());
        verify(gamePlayerDaoMock, times(1)).getPlayerCount("Game");
    }

    @Test
    public void getRoundRobinRankingTest() throws InterruptedException{
        LiveData<List<GamePlayerCrossRef>> gameStatus = createLiveDataStatus();
        when(gamePlayerDaoMock.getRankingByPoints("Game")).thenReturn(gameStatus);
        GameStatusRepository gameStatusRepository = new GameStatusRepository(db);
        LiveData<List<GamePlayerCrossRef>> fetchedData =
                gameStatusRepository.getRoundRobinRanking("Game");

        verify(gamePlayerDaoMock, times(1)).getRankingByPoints("Game");

        assertTestData(fetchedData);
    }

    @Test
    public void getDoubleEliminationPlayerCountTest() throws InterruptedException {
        LiveData<Integer> playerCount = new MutableLiveData<>(3);
        when(gamePlayerDaoMock.getDoubleEliminationPlayerCount("Game")).thenReturn(playerCount);
        GameStatusRepository gameStatusRepository = new GameStatusRepository(db);
        LiveData<Integer> count = gameStatusRepository.getDoubleEliminationPlayerCount("Game");

        verify(gamePlayerDaoMock, times(1)).getDoubleEliminationPlayerCount("Game");

        Assert.assertEquals(3, LiveDataTestHelper.getLiveDataValue(count).intValue());
    }

    @Test
    public void getDoubleEliminationRankingTest() throws  InterruptedException {
        LiveData<List<GamePlayerCrossRef>> gameStatus = createLiveDataStatus();
        when(gamePlayerDaoMock.getRankingDoubleElimination("Game")).thenReturn(gameStatus);
        GameStatusRepository gameStatusRepository = new GameStatusRepository(db);
        LiveData<List<GamePlayerCrossRef>> fetchedData =
                gameStatusRepository.getDoubleEliminationRanking("Game");

        verify(gamePlayerDaoMock, times(1)).getRankingDoubleElimination("Game");


        assertTestData(fetchedData);
    }

    @Test
    public void getGameStatusTest() throws InterruptedException{
        LiveData<List<GamePlayerCrossRef>> gameStatus = createLiveDataStatus();
        when(gamePlayerDaoMock.getGameStatus("Game")).thenReturn(gameStatus);
        GameStatusRepository gameStatusRepository = new GameStatusRepository(db);
        LiveData<List<GamePlayerCrossRef>> fetchedData =
                gameStatusRepository.getGameStatus("Game");

        verify(gamePlayerDaoMock, times(1)).getGameStatus("Game");

        assertTestData(fetchedData);
    }

    @Test
    public void loadGameDataTest() throws InterruptedException{
        LiveData<List<String>> gameData = new MutableLiveData<>(List.of("player1", "player2", "player3"));
        when(gamePlayerDaoMock.loadGameData("Game")).thenReturn(gameData);
        GameStatusRepository gameStatusRepository = new GameStatusRepository(db);
        LiveData<List<String>> fetchedData =
                gameStatusRepository.loadGameData("Game");

        verify(gamePlayerDaoMock, times(1)).loadGameData("Game");

        Assert.assertEquals("player1", LiveDataTestHelper.getLiveDataValue(gameData).get(0));
        Assert.assertEquals("player2", LiveDataTestHelper.getLiveDataValue(gameData).get(1));
        Assert.assertEquals("player3", LiveDataTestHelper.getLiveDataValue(gameData).get(2));
    }

    @Test
    public void insertPlayerOfGameTest(){
        List<PlayerHelper> players = List.of(
                new PlayerHelper("Name1", "Uri1"),
                new PlayerHelper("Name2", "Uri2"));

        GameStatusRepository gameStatusRepository = new GameStatusRepository(db);
        gameStatusRepository.insertPlayersOfGame(players, "Game");
        verify(gamePlayerDaoMock, times(2)).insert(any());
    }

    @Test
    public void increaseTimesMatchedTest() throws InterruptedException {
        GameStatusRepository gameStatusRepository = new GameStatusRepository(db);
        gameStatusRepository.increaseTimesMatched("Game", "Player");

        verify(gamePlayerDaoMock,
                times(1)).addMatch(any(), any());
    }

    @Test
    public void addPointsTest() throws InterruptedException {
        GameStatusRepository gameStatusRepository = new GameStatusRepository(db);
        gameStatusRepository.addPoints("Game", "Player", 3);

        verify(gamePlayerDaoMock,
                times(1)).addPoints("Game", "Player", 3);
    }

}