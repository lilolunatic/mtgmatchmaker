package com.example.mtgmatchmaker.db.daos;

import android.content.Context;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import com.example.mtgmatchmaker.LiveDataTestHelper;
import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.daos.GameDao;
import com.example.mtgmatchmaker.db.daos.GameWithPlayerDao;
import com.example.mtgmatchmaker.db.daos.PlayerDao;
import com.example.mtgmatchmaker.db.pojos.Game;
import com.example.mtgmatchmaker.db.pojos.GamePlayerCrossRef;
import com.example.mtgmatchmaker.db.pojos.Player;
import org.junit.*;

import java.io.IOException;
import java.util.List;

public class GameWithPlayerDaoTest {
    private GameWithPlayerDao gameWithPlayerDao;
    private GameDao gameDao;
    private PlayerDao playerDao;
    private MatchmakerDatabase db;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void createDb(){
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, MatchmakerDatabase.class).build();
        gameWithPlayerDao = db.gamesWithPlayersDao();
        gameDao = db.gameDao();
        playerDao = db.playerDao();
    }

    @After
    public void closeDb() throws IOException{
        db.close();
    }

    private void insertTestData(){
        GamePlayerCrossRef player1 = new GamePlayerCrossRef("player1", "game1", "pictureUri1");
        GamePlayerCrossRef player2 = new GamePlayerCrossRef("player2", "game1", "pictureUri2");
        GamePlayerCrossRef player3 = new GamePlayerCrossRef("player3", "game2", "pictureUri2");
        GamePlayerCrossRef player4 = new GamePlayerCrossRef("player4", "game2", "pictureUri2");
        GamePlayerCrossRef player5 = new GamePlayerCrossRef("player5", "game2", "pictureUri2");
        gameDao.insert(new Game("game1", true));
        gameDao.insert(new Game("game2", false));
        playerDao.insert(new Player("player1"));
        playerDao.insert(new Player("player2"));
        playerDao.insert(new Player("player3"));
        playerDao.insert(new Player("player4"));
        playerDao.insert(new Player("player5"));
        gameWithPlayerDao.insert(player1);
        gameWithPlayerDao.insert(player2);
        gameWithPlayerDao.insert(player3);
        gameWithPlayerDao.insert(player4);
        gameWithPlayerDao.insert(player5);
    }

    @Test
    public void insertGetGameStatus() throws InterruptedException {
        insertTestData();
        LiveData<List<GamePlayerCrossRef>> ldGame = gameWithPlayerDao.getGameStatus("game1");
        List<GamePlayerCrossRef> game = LiveDataTestHelper.getLiveDataValue(ldGame);
        Assert.assertEquals(game.get(0).getGameName(), "game1");
        Assert.assertEquals(game.get(0).getPlayerName(), "player1");
        Assert.assertEquals(game.get(0).getPoints(), 0);
        Assert.assertEquals(game.get(0).getTimesMatched(), 0);
    }

    @Test
    public void loadGameDataTest() throws InterruptedException{
        insertTestData();
        LiveData<List<String>> ldPlayerNames = gameWithPlayerDao.loadGameData("game2");
        List<String> playerNames = LiveDataTestHelper.getLiveDataValue(ldPlayerNames);
        Assert.assertEquals(playerNames.get(0), "player3");
        Assert.assertEquals(playerNames.get(1), "player4");
        Assert.assertEquals(playerNames.get(2), "player5");
    }

    @Test
    public void addPointsTest() throws InterruptedException{
        insertTestData();
        gameWithPlayerDao.addPoints("game1", "player1", 2);
        LiveData<List<GamePlayerCrossRef>> ldGame = gameWithPlayerDao.getGameStatus("game1");
        List<GamePlayerCrossRef> game = LiveDataTestHelper.getLiveDataValue(ldGame);
        Assert.assertEquals(game.get(0).getPlayerName(), "player1");
        Assert.assertEquals(game.get(1).getPlayerName(), "player2");
        Assert.assertEquals(game.get(0).getPoints(), 2);
        Assert.assertEquals(game.get(1).getPoints(), 0);
    }

    @Test
    public void addMatchTest() throws InterruptedException{
        insertTestData();
        gameWithPlayerDao.addMatch("game2", "player3");
        LiveData<List<GamePlayerCrossRef>> ldGame = gameWithPlayerDao.getGameStatus("game2");
        List<GamePlayerCrossRef> game = LiveDataTestHelper.getLiveDataValue(ldGame);
        Assert.assertEquals(game.get(0).getPlayerName(), "player3");
        Assert.assertEquals(game.get(1).getPlayerName(), "player4");
        Assert.assertEquals(game.get(0).getTimesMatched(), 1);
        Assert.assertEquals(game.get(1).getTimesMatched(), 0);
    }

    @Test
    public void getRankingByPointsTest() throws InterruptedException{
        insertTestData();
        gameWithPlayerDao.addPoints("game2", "player3", 1);
        gameWithPlayerDao.addPoints("game2", "player4", 2);
        gameWithPlayerDao.addPoints("game2", "player5", 3);
        LiveData<List<GamePlayerCrossRef>> ldRanking = gameWithPlayerDao.getRankingByPoints("game2");
        List<GamePlayerCrossRef> ranking = LiveDataTestHelper.getLiveDataValue(ldRanking);
        Assert.assertEquals(ranking.get(0).getPlayerName(), "player5");
        Assert.assertEquals(ranking.get(1).getPlayerName(), "player4");
        Assert.assertEquals(ranking.get(2).getPlayerName(), "player3");
    }

    @Test
    public void getRankingDoubleEliminationTest() throws InterruptedException{
        insertTestData();
        gameWithPlayerDao.addPoints("game2", "player3", 1);
        gameWithPlayerDao.addPoints("game2", "player4", 2);
        gameWithPlayerDao.addPoints("game2", "player5", 1);
        gameWithPlayerDao.addMatch("game2", "player3");
        gameWithPlayerDao.addMatch("game2", "player3");
        gameWithPlayerDao.addMatch("game2", "player3");
        gameWithPlayerDao.addMatch("game2", "player4");
        gameWithPlayerDao.addMatch("game2", "player4");
        gameWithPlayerDao.addMatch("game2", "player5");
        gameWithPlayerDao.addMatch("game2", "player5");
        LiveData<List<GamePlayerCrossRef>> ldRanking = gameWithPlayerDao.getRankingDoubleElimination("game2");
        List<GamePlayerCrossRef> ranking = LiveDataTestHelper.getLiveDataValue(ldRanking);
        Assert.assertEquals(ranking.get(0).getPlayerName(), "player3");
        Assert.assertEquals(ranking.get(1).getPlayerName(), "player5");
        Assert.assertEquals(ranking.get(2).getPlayerName(), "player4");
    }

    @Test
    public void getPlayerCountTest() throws InterruptedException{
        insertTestData();
        LiveData<Integer> ldCount = gameWithPlayerDao.getPlayerCount("game2");
        int count = LiveDataTestHelper.getLiveDataValue(ldCount);
        Assert.assertEquals(count, 3);

    }

    @Test
    public void getDoubleEliminationCountTest() throws InterruptedException{
        insertTestData();
        gameWithPlayerDao.addPoints("game2", "player3", 2);
        LiveData<Integer> ldCount = gameWithPlayerDao.getDoubleEliminationPlayerCount("game2");
        int count = LiveDataTestHelper.getLiveDataValue(ldCount);
        Assert.assertEquals(count, 2);
    }
}