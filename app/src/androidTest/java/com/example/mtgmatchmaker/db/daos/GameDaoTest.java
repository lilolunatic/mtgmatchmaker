package com.example.mtgmatchmaker.db.daos;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.mtgmatchmaker.LiveDataTestHelper;
import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.pojos.Game;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

@RunWith(AndroidJUnit4.class)
public class GameDaoTest {
    private GameDao gameDao;
    private MatchmakerDatabase db;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void createDb(){
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, MatchmakerDatabase.class).build();
        gameDao = db.gameDao();
    }

    @After
    public void closeDB() throws IOException{
        db.close();
    }

    @Test
    public void insertGetAllGamesTest() throws Exception{
        Game game1 = new Game("game1", true);
        Game game2 = new Game("game2", false);
        gameDao.insert(game1);
        gameDao.insert(game2);
        LiveData<List<Game>> dbGames = gameDao.getOrderedGames();
        List<Game> foundGames = LiveDataTestHelper.getLiveDataValue(dbGames);
        Assert.assertEquals("game1", foundGames.get(0).getGameName());
        Assert.assertEquals(true, foundGames.get(0).isRoundRobin());
        Assert.assertEquals(0, foundGames.get(0).getRoundCount());
        Assert.assertEquals("game2", foundGames.get(1).getGameName());
        Assert.assertEquals(false, foundGames.get(1).isRoundRobin());
        Assert.assertEquals(0, foundGames.get(1).getRoundCount());

    }

    @Test
    public void getGameByNameTest() throws Exception{
        Game game1 = new Game("game1", true);
        Game game2 = new Game("game2", false);
        Game target = new Game("target", true);
        gameDao.insert(game1);
        gameDao.insert(game2);
        gameDao.insert(target);
        LiveData<Game> ldTargetGame = gameDao.getGame("target");
        Game targetGame = LiveDataTestHelper.getLiveDataValue(ldTargetGame);
        Assert.assertEquals("target", targetGame.getGameName());
        Assert.assertEquals(true, targetGame.isRoundRobin());
    }

    @Test
    public void increaseGetRoundCountTest() throws Exception{
        Game game = new Game("game", false);
        gameDao.insert(game);
        LiveData<Game> ldGame = gameDao.getGame("game");
        Game foundGame = LiveDataTestHelper.getLiveDataValue(ldGame);
        Assert.assertEquals(0, foundGame.getRoundCount());
        gameDao.nextRound("game");
        LiveData<Game> ldIncreasedGame = gameDao.getGame("game");
        Game foundIncreasedGame = LiveDataTestHelper.getLiveDataValue(ldIncreasedGame);
        Assert.assertEquals(1, foundIncreasedGame.getRoundCount());
    }
}