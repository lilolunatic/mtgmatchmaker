package com.example.mtgmatchmaker.db.repositories;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mtgmatchmaker.LiveDataTestHelper;
import com.example.mtgmatchmaker.db.MatchmakerDatabase;
import com.example.mtgmatchmaker.db.daos.GameDao;
import com.example.mtgmatchmaker.db.pojos.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameRepositoryTest {
    private MatchmakerDatabase db;
    private GameDao gameDaoMock;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        db = Mockito.mock(MatchmakerDatabase.class);
        gameDaoMock = Mockito.mock(GameDao.class);
        when(db.gameDao()).thenReturn(gameDaoMock);
    }

    @Test
    public void getEmptyGamesListTest() throws InterruptedException {
        when(gameDaoMock.getOrderedGames()).thenReturn(new MutableLiveData<>(new ArrayList<>()));

        GameRepository gameRepository = new GameRepository(db);
        Assert.assertEquals(0, LiveDataTestHelper.getLiveDataValue(gameRepository.getAllGames()).size());
    }

    private List<Game> createGamesList(){
        Game aGame = new Game("aGame", true);
        Game bGame = new Game("bGame", false);
        Game cGame = new Game("cGame", false);

        List<Game> gameList = List.of(aGame, bGame, cGame);
        return gameList;
    }

    private LiveData<List<Game>> createLiveDataGameList(){
        List<Game> gameList = createGamesList();
        MutableLiveData games = new MutableLiveData<List<Game>>(gameList);
        return games;
    }

    @Test
    public void insertGamesTest() {
        GameRepository gameRepository = new GameRepository(db);
        Game aGame = new Game("aGame", true);
        Game bGame = new Game("bGame", false);

        gameRepository.insert(aGame);
        gameRepository.insert(bGame);

        verify(gameDaoMock).insert(aGame);
        verify(gameDaoMock).insert(bGame);
    }

    @Test
    public void getAllGamesTest() throws InterruptedException{
        List<Game> gameList = createGamesList();
        LiveData<List<Game>> games = createLiveDataGameList();
        when(gameDaoMock.getOrderedGames()).thenReturn(games);
        GameRepository gameRepository = new GameRepository(db);

        LiveData<List<Game>> fetchedGames = gameRepository.getAllGames();
        Assert.assertEquals(3, LiveDataTestHelper.getLiveDataValue(fetchedGames).size());
    }

    @Test
    public void getGameTest() throws InterruptedException{
        Game aGame = new Game("aGame", true);
        when(gameDaoMock.getGame("aGame")).thenReturn(new MutableLiveData<>(aGame));

        GameRepository gameRepository = new GameRepository(db);
        LiveData<Game> fetchedGame = gameRepository.getGame("aGame");
        verify(gameDaoMock, times(1)).getGame("aGame");
        Assert.assertEquals("aGame", LiveDataTestHelper.getLiveDataValue(fetchedGame).getGameName());
        Assert.assertEquals(true, LiveDataTestHelper.getLiveDataValue(fetchedGame).isRoundRobin());
    }

    @Test
    public void increaseRoundCountTest() {
        Game game = new Game("aGame", true);
        GameRepository gameRepository = new GameRepository(db);

        gameRepository.increaseRoundCount(game.getGameName());

        verify(gameDaoMock).nextRound(game.getGameName());
    }

    @Test
    public void getRoundCount() throws InterruptedException{
        GameRepository gameRepository = new GameRepository(db);
        MutableLiveData<Integer> roundCount = new MutableLiveData<>(3);
        when(gameDaoMock.getRoundCount("aGame")).thenReturn(roundCount);

        Assert.assertEquals(3, LiveDataTestHelper.getLiveDataValue(
                gameRepository.getRoundCount("aGame")).intValue());
    }
}