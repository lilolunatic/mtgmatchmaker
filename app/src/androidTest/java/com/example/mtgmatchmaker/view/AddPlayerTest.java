package com.example.mtgmatchmaker.view;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.example.mtgmatchmaker.R;
import com.example.mtgmatchmaker.view.start.StartActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AddPlayerTest {

    @Rule
    public ActivityTestRule<StartActivity> mActivityTestRule = new ActivityTestRule<>(StartActivity.class);

    @Test
    public void addPlayerTest() {
        ViewInteraction appCompatButton = onView(
allOf(withId(R.id.newGameButton), withText("New Game"),
childAtPosition(
childAtPosition(
withId(android.R.id.content),
0),
1),
isDisplayed()));
        appCompatButton.perform(click());
        
        ViewInteraction textInputEditText = onView(
allOf(withId(R.id.gameNameTextInput),
childAtPosition(
childAtPosition(
withId(android.R.id.content),
0),
1),
isDisplayed()));
        textInputEditText.perform(replaceText("addPlayer"), closeSoftKeyboard());
        
        ViewInteraction appCompatButton2 = onView(
allOf(withId(R.id.addPlayerButton), withText("Add"),
childAtPosition(
childAtPosition(
withId(android.R.id.content),
0),
4),
isDisplayed()));
        appCompatButton2.perform(click());
        
        ViewInteraction appCompatEditText = onView(
allOf(withId(R.id.TextInputPlayerName),
childAtPosition(
childAtPosition(
withId(R.id.custom),
0),
0),
isDisplayed()));
        appCompatEditText.perform(replaceText("A"), closeSoftKeyboard());
        
        ViewInteraction appCompatButton3 = onView(
allOf(withId(android.R.id.button1), withText("Done!"),
childAtPosition(
childAtPosition(
withId(R.id.buttonPanel),
0),
3)));
        appCompatButton3.perform(scrollTo(), click());
        
        ViewInteraction textView = onView(
allOf(withId(R.id.playerName), withText("A"),
withParent(withParent(withId(R.id.rv_players))),
isDisplayed()));
        textView.check(matches(withText("A")));
        
        ViewInteraction button = onView(
allOf(withId(R.id.gameCreationFinishButton), withText("SAVE"),
withParent(withParent(withId(android.R.id.content))),
isDisplayed()));
        button.check(matches(isDisplayed()));
        
        ViewInteraction appCompatButton4 = onView(
allOf(withId(R.id.addPlayerButton), withText("Add"),
childAtPosition(
childAtPosition(
withId(android.R.id.content),
0),
4),
isDisplayed()));
        appCompatButton4.perform(click());
        
        ViewInteraction appCompatEditText2 = onView(
allOf(withId(R.id.TextInputPlayerName),
childAtPosition(
childAtPosition(
withId(R.id.custom),
0),
0),
isDisplayed()));
        appCompatEditText2.perform(replaceText("B"), closeSoftKeyboard());
        
        ViewInteraction appCompatButton5 = onView(
allOf(withId(android.R.id.button1), withText("Done!"),
childAtPosition(
childAtPosition(
withId(R.id.buttonPanel),
0),
3)));
        appCompatButton5.perform(scrollTo(), click());
        
        ViewInteraction textView2 = onView(
allOf(withId(R.id.playerName), withText("B"),
withParent(withParent(withId(R.id.rv_players))),
isDisplayed()));
        textView2.check(matches(withText("B")));
        
        ViewInteraction textView3 = onView(
allOf(withId(R.id.playerName), withText("B"),
withParent(withParent(withId(R.id.rv_players))),
isDisplayed()));
        textView3.check(matches(withText("B")));
        
        ViewInteraction appCompatButton6 = onView(
allOf(withId(R.id.gameCreationFinishButton), withText("Save"),
childAtPosition(
childAtPosition(
withId(android.R.id.content),
0),
3),
isDisplayed()));
        appCompatButton6.perform(click());
        
        ViewInteraction textView4 = onView(
allOf(withId(R.id.namePlayer1), withText("A"),
withParent(withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class))),
isDisplayed()));
        textView4.check(matches(withText("A")));
        
        ViewInteraction textView5 = onView(
allOf(withId(R.id.pointsPlayer1), withText("0"),
withParent(withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class))),
isDisplayed()));
        textView5.check(matches(withText("0")));
        
        ViewInteraction textView6 = onView(
allOf(withId(R.id.namePlayer2), withText("B"),
withParent(withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class))),
isDisplayed()));
        textView6.check(matches(withText("B")));
        
        ViewInteraction textView7 = onView(
allOf(withId(R.id.pointsPlayer2), withText("0"),
withParent(withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class))),
isDisplayed()));
        textView7.check(matches(withText("0")));
        
        ViewInteraction textView8 = onView(
allOf(withId(R.id.pointsPlayer2), withText("0"),
withParent(withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class))),
isDisplayed()));
        textView8.check(matches(withText("0")));
        }
    
    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup)parent).getChildAt(position));
            }
        };
    }
    }
