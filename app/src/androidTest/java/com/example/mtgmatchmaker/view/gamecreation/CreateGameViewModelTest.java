package com.example.mtgmatchmaker.view.gamecreation;

import android.app.Application;
import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.core.app.ApplicationProvider;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class CreateGameViewModelTest {
    private Context context;
    private Application application;

    @Before
    public void setUp() throws Exception {
        context = ApplicationProvider.getApplicationContext();
        application = ApplicationProvider.getApplicationContext();
    }

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Test
    public void addPlayerToListTest() {
        PlayerHelper player1 = new PlayerHelper("player1", "pictureUri1");
        PlayerHelper player2 = new PlayerHelper("player2", "pictureUri2");

        CreateGameViewModel viewModel = new CreateGameViewModel(application);
        viewModel.addPlayerToList(player1);
        viewModel.addPlayerToList(player2);

        Assert.assertEquals(2, viewModel.getAddedPlayers().size());
        Assert.assertEquals("player1", viewModel.getAddedPlayers().get(0).getPlayerName());
        Assert.assertEquals("player2", viewModel.getAddedPlayers().get(1).getPlayerName());
    }

    @Test
    public void removePlayerFromList() {
        PlayerHelper player1 = new PlayerHelper("player1", "pictureUri1");
        PlayerHelper player2 = new PlayerHelper("player2", "pictureUri2");

        CreateGameViewModel viewModel = new CreateGameViewModel(application);
        viewModel.addPlayerToList(player1);
        viewModel.addPlayerToList(player2);
        viewModel.removePlayerFromList(0);

        Assert.assertEquals(1, viewModel.getAddedPlayers().size());
        Assert.assertEquals("player2", viewModel.getAddedPlayers().get(0).getPlayerName());
    }

    @Test
    public void setPictureOfPlayer() {
        PlayerHelper player1 = new PlayerHelper("player1", "pictureUri1");

        CreateGameViewModel viewModel = new CreateGameViewModel(application);
        viewModel.addPlayerToList(player1);
        viewModel.setPictureOfPlayer(0, "This is the new picture");

        Assert.assertEquals("This is the new picture",
                viewModel.getAddedPlayers().get(0).getPictureUri());
    }

    @Test
    public void getPlayerNames() {
        PlayerHelper player1 = new PlayerHelper("player1", "pictureUri1");
        PlayerHelper player2 = new PlayerHelper("player2", "pictureUri1");
        PlayerHelper player3 = new PlayerHelper("player3", "pictureUri1");

        CreateGameViewModel viewModel = new CreateGameViewModel(application);
        viewModel.addPlayerToList(player1);
        viewModel.addPlayerToList(player2);
        viewModel.addPlayerToList(player3);

        String[] playerNames = viewModel.getPlayerNames();

        Assert.assertEquals("player1", viewModel.getPlayerNames()[0]);
        Assert.assertEquals("player2", viewModel.getPlayerNames()[1]);
        Assert.assertEquals("player3", viewModel.getPlayerNames()[2]);
    }
}