package com.example.mtgmatchmaker;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class LiveDataTestHelper {
    public static <T> T getLiveDataValue(final LiveData<T> liveData) throws InterruptedException{
        final Object[] data = new Object[1];
        final CountDownLatch latch = new CountDownLatch(1);
        Observer<T> observer = new Observer<T>(){
            @Override
            public void onChanged(@Nullable T o){
                data[0] = o;
                latch.countDown();
                liveData.removeObserver(this);
            }
        };
        liveData.observeForever(observer);
        if(!latch.await(2, TimeUnit.SECONDS)){
            throw new RuntimeException("LiveData not set");
        }
        return (T) data[0];
    }
}
